/* 
 * File:   main.cpp
 * Author: Junior Cesar Abreu <juniorcesarabreu@live.com>
 *
 * Created on 6 de Setembro de 2016, 09:04
 * 
 * This project requires compiler and library support for the ISO C++ 2011
 * This project can be opened in Netbeans and Codeblocks
 */

/*
 * Pij      MxN     [1, 150]
 * Sijk     MxNxN   DU[1, 50]
 * Rj       1xN     DU[0, 60]
 * dj       1xN     (Q + (M / 10)) * DU[0.3 * (P + S), 2 * (P + S)]
 *                  Q = 2 para gerar datas de entregas apertadas
 *                  M numero de máquinas
 *                  P tempo de processamento
 *                  S tempo de configuração
 * 
 *  4 problemas teste
 *  2 medios    (N = 50, M = 5)
 *              (N = 30, M = 3)
 * 
 *  2 grandes   (N = 80, M = 8)
 *              (N = 100, M = 10)
 */

#include <cstdlib>
#include <sys/unistd.h>
#include <stdio.h>
#include <random> // geração de numero aleatorios com distribuição uniforme

void geraInstancia(int n, int m);
int geraNumRand(int limMin, int limMax);
int estimaDataEntrega(int Q, int M, int j);
void exibeInstancia();
void imprimeInstancia();
int calculaMediaTempoProcessamento(int j);
int calculaMediaTempoPreparacao(int j);

using namespace std;

int n, m;

int Pij[10][100];
int Sijk[10][100][100];
int Rj[100];
int dj[100];

FILE *arq;

int main(int argc, char** argv) {

    n = 10;
    m = 10;

    geraInstancia(n, m);

    exibeInstancia();
    
    imprimeInstancia();

    return 0;
}

void geraInstancia(int n, int m) {

    // Tempo de Processamento Pij
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            Pij[i][j] = geraNumRand(1, 150);
        }
    }

    // Tempo de Preparação Sijk
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                if(j == k) Sijk[i][j][k] = 0;
                else Sijk[i][j][k] = geraNumRand(1, 50);
            }
        }
    }

    // Tempo de Liberação Rj
    for (int j = 0; j < n; j++) {
        Rj[j] = geraNumRand(0, 60);
    }

    // Data de entrega dj
    for (int j = 0; j < n; j++) {
        dj[j] = estimaDataEntrega(2, m, j);
    }
}

///Discrete Uniform Distribution
int geraNumRand(int const intervaloDe, int const intervaloAte) {
//http://stackoverflow.com/questions/288739/generate-random-numbers-uniformly-over-an-entire-range/288869#288869
//http://stackoverflow.com/a/20136256
//http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
    
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(intervaloDe, intervaloAte);

    //std::cout << distr(generator) << '\n';
    return distr(generator);
}

int estimaDataEntrega(int Q, int M, int j) {
    
    int P = calculaMediaTempoProcessamento(j);/*cada tarefa possui 1 tempo de processamento por máquina*/
    int S = calculaMediaTempoPreparacao(j); /*cada tarefa possui 1 tempo de preparação por máquina e após suceder alguma outra tarefa*/

    return (Q + (M / 10)) * geraNumRand(0.3 * (P + S), 2 * (P + S));
}

int calculaMediaTempoProcessamento(int j){
    
    double soma = 0;
    for(int i = 0; i < m; i++){
        soma += Pij[i][j];
    }    
    return soma/m;
}

int calculaMediaTempoPreparacao(int j){
    /*execução na máquina i da tarefa j após a tarefa k*/
    double soma = 0;
    for(int i = 0; i < m; i++){
        for(int k = 0; k < n; k++){
            if(j == k) continue;
            soma += Sijk[i][j][k];
        }
    }    
    return soma/(m*(n-1));
}
void exibeInstancia() {

    // Tempo de Processamento Pij
    printf("Tempo de processamento Pij \n");
    printf("\t     ");
    for (int j = 0; j < n; j++) printf(" tar %03d", j+1);
    printf("\n");
    for (int i = 0; i < m; i++) {
        printf("maquina %03d: ", i+1);
        for (int j = 0; j < n; j++) {
            printf("\t%03d", Pij[i][j]);
        }
        printf("\n");
    }

    // Tempo de Preparação Sijk
    printf("\nTempo de preparacao Sijk \n");
    for (int i = 0; i < m; i++) {
        printf("maquina %03d: \n", i+1);

        printf("\t\t     ");
        for (int j = 0; j < n; j++) printf(" tar %03d", j+1);
        printf("\n");

        for (int j = 0; j < n; j++) {
            printf("\t tarefa %03d: ", j+1);

            for (int k = 0; k < n; k++) {
                printf("\t%03d", Sijk[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }

    // Tempo de Liberação Rj
    printf("Tempo de liberacao Rj \n");
    printf("     ");
    for (int j = 0; j < n; j++) printf(" tar %03d", j+1);
    printf("\n");
    for (int j = 0; j < n; j++) {
        printf("\t%03d", Rj[j]);
    }
    printf("\n\n");

    // Data de entrega dj
    printf("Data de entrega dj \n");
    printf("     ");
    for (int j = 0; j < n; j++) printf(" tar %03d", j+1);
    printf("\n");
    for (int j = 0; j < n; j++) {
        printf("\t%03d", dj[j]);
    }
    
}

void imprimeInstancia() {
    
    arq = fopen("inst-gen.txt", "w");
    
    // numero de tarefas e máquinas
    fprintf(arq, "# Numero de tarefas e máquinas \n");
    fprintf(arq, "%d %d\n", n, m);

    // Tempo de Processamento Pij
    fprintf(arq, "# Tempo de processamento Pij \n");
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            fprintf(arq, "%03d ", Pij[i][j]);
        }
        fprintf(arq, "\n");
    }

    // Tempo de Preparação Sijk
    fprintf(arq, "# Tempo de preparacao Sijk \n");
    for (int i = 0; i < m; i++) {
        fprintf(arq, "maquina: %03d \n", i+1);

        for (int j = 0; j < n; j++) {

            for (int k = 0; k < n; k++) {
                fprintf(arq, "%03d ", Sijk[i][j][k]);
            }
            fprintf(arq, "\n");
        }
    }

    // Tempo de Liberação Rj
    fprintf(arq, "# Tempo de liberacao Rj \n");
    for (int j = 0; j < n; j++) {
        fprintf(arq, "%03d ", Rj[j]);
    }
    fprintf(arq, "\n");

    // Data de entrega dj
    fprintf(arq, "# Data de entrega dj \n");
    for (int j = 0; j < n; j++) {
        fprintf(arq, "%03d ", dj[j]);
    }
    fprintf(arq, "\n");
    
    fclose(arq);

}